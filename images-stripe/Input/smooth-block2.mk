# Rescale images (block and smoothing)
#
# This make file is written to save g r i images in with different pixel size and smoothed.
# Input images are in the Source folder of the corresponding field, the output images will be saved in 
# different directories.
# This script works specifically for a scale of 0.5 and a smoothing with a moffattian function.


#
# Original author:
# Giulia Golini  <giulia.golini@gmail.com>
# Contributing author(s): Giulia Golini  <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

.ONESHELL:

indir = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source

outdir_b2_smooth = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/build_b2_smooth
k-outdir = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/build_smooth/kernel



all: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_smooth-block.txt


$(outdir_b2_smooth): ; mkdir $@


	
$(outdir_b2_smooth)/f1213_g_block2_smooth.fits : $(indir)/f1213_g.rec_subtracted_it2_it3.fits $(k-outdir)/kernel.fits
#/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_g_block2.fits: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source/f1213_g.rec_subtracted_it2_it3.fits 
	astwarp $(word 1,$^) --hdu=0 --scale=0.5 --output=$$temp.fits
	astconvolve $$temp.fits -h1 --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@



$(outdir_b2_smooth)/f1213_r_block2_smooth.fits : $(indir)/f1213_r.rec_subtracted_it2_it3.fits $(k-outdir)/kernel.fits
	astwarp $(word 1,$^) --hdu=0 --scale=0.5 --output=$$temp.fits
	astconvolve $$temp.fits -h1 --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@


$(outdir_b2_smooth)/f1213_i_block2_smooth.fits : $(indir)/f1213_i.rec_subtracted_it2_it3.fits $(k-outdir)/kernel.fits
	astwarp $(word 1,$^) --hdu=0 --scale=0.5 --output=$$temp.fits
	astconvolve $$temp.fits -h1 --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@



/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_smooth-block.txt: $(outdir_b2_smooth) $(outdir_b2_smooth)/f1213_g_block2_smooth.fits $(outdir_b2_smooth)/f1213_r_block2_smooth.fits $(outdir_b2_smooth)/f1213_i_block2_smooth.fits
	echo $< >  $@
