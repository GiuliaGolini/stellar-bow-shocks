# Rescale images (different block)
#
# This make file is written to save g r i images in with different pixel size.
# Input images are in the Source folder of the corresponding field, the output images will be saved in 
# different directories.
# This script works specifically for a scale of 0.5


#
# Original author:
# Giulia Golini  <giulia.golini@gmail.com>
# Contributing author(s): Giulia Golini  <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

.ONESHELL:

indir = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source

outdir_b2 = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/build_b2




all: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_rescaled.txt


$(outdir_b2): ; mkdir $@


	
$(outdir_b2)/f1213_g_block2.fits : $(indir)/f1213_g.rec_subtracted_it2_it3.fits
#/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_g_block2.fits: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source/f1213_g.rec_subtracted_it2_it3.fits 
	astwarp $(word 1,$^) --hdu=0 --scale=0.5 --output=$@


$(outdir_b2)/f1213_r_block2.fits : $(indir)/f1213_r.rec_subtracted_it2_it3.fits
	astwarp $(word 1,$^) --hdu=0 --scale=0.5 --output=$@


$(outdir_b2)/f1213_i_block2.fits : $(indir)/f1213_i.rec_subtracted_it2_it3.fits
	astwarp $(word 1,$^) --hdu=0 --scale=0.5 --output=$@



/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_rescaled.txt: $(outdir_b2) $(outdir_b2)/f1213_g_block2.fits $(outdir_b2)/f1213_r_block2.fits $(outdir_b2)/f1213_i_block2.fits
	echo $< >  $@
