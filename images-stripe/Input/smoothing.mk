# Smooth images 
#
# This make file is written to save smoothed g r i images.
# Input images are in the Source folder of the corresponding field, the output images will be saved in 
# different directories.
# To smooth the image I use a molfattian kernel function.
# This script works for only one field!


#
# Original author:
# Giulia Golini  <giulia.golini@gmail.com>
# Contributing author(s): Giulia Golini  <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

.ONESHELL:

indir = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source

outdir_smooth = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/build_smooth

k-outdir = /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/build_smooth/kernel


all: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_smoothed.txt



$(outdir_smooth): ; mkdir $@





$(k-outdir):; mkdir $@
$(k-outdir)/kernel.fits : | $(k-outdir)
	astmkprof --kernel=moffat,3,2.8,5 --oversample=1 --output=$@ 




	
$(outdir_smooth)/f1213_g_smoothed.fits : $(indir)/f1213_g.rec_subtracted_it2_it3.fits $(k-outdir)/kernel.fits
	astconvolve $(word 1,$^) -h0 --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@



$(outdir_smooth)/f1213_r_smoothed.fits : $(indir)/f1213_r.rec_subtracted_it2_it3.fits $(k-outdir)/kernel.fits
	astconvolve $(word 1,$^) -h0 --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@

	
$(outdir_smooth)/f1213_i_smoothed.fits : $(indir)/f1213_i.rec_subtracted_it2_it3.fits $(k-outdir)/kernel.fits
	astconvolve $(word 1,$^) -h0 --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@



/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_smoothed.txt: $(outdir_smooth) $(k-outdir) $(k-outdir)/kernel.fits  $(outdir_smooth)/f1213_g_smoothed.fits $(outdir_smooth)/f1213_r_smoothed.fits $(outdir_smooth)/f1213_i_smoothed.fits
	echo $< >  $@
