
"""

Date : May 2018
Author : Nushkia Chamba (chamba@iac.es)
Affiliation : Instituto de Astrofisica de Canarias, Tenerife, Spain

Creates a scaled, transformed RGB composite image using the i, r and g-band
astronomical data following the method of Lupton et al. (2004).
(https://arxiv.org/abs/astro-ph/0312483)

This Python version is based on Lee Kelvin's R "astro" library
(https://cran.r-project.org/web/packages/astro/index.html).
Note that the three images must be aligned and have the same pixel scale and size.


"""



''' Giulia Golini <giulia.golini@gmail.com>  Notes:
    This python code is written to work well with photometrized images with around 1 to 100 counts/pixel.
    Check the pixels values of the stacked input images.
    In my case, due to very deep observations, one way should be using a zero point around 26/27.
    However I decided to use the ssds zeropoint at 22.5 and, before running this script multiply my
    stacked images for a factor 1e5.
    Another way, just for style, could be doing a block with astwarp and a scale of 0.25 ( block 4 )
    of the images before sending it to this code. '''




import numpy as np
from astropy.io import fits
import matplotlib.image
import sys

def ascale(image, start = 0., stop = 1., lo = None, hi = None, hard = False):

    ''' Rescales an image matrix onto a new linear number scale.
    If the option 'hard' is set True, numeric values which lie outside the
    range [lo,hi] will be set to these hard limits.

    Params
    ------
    image (np.ndarray) :
        image data to be rescaled
    start (np.float) :
        lower bound of new rescaled data. Default: np.nanmin(image)
    stop (np.float) :
        upper bound of new rescaled data. Default: np.nanmax(image)
    lo (np.float) :
        lower cut reference level of original data
    hi (np.float) :
        upper cut reference level of original data
    hard (boolean value) :
        impose hard limits? True or False

    Returns
    -------
    rescale_image (np.ndarray) :
        new rescaled image with the same shape as image

    '''

    if lo is None or hi is None:
        lo = np.nanmin(image)
        hi = np.nanmax(image)
    # scale data to range [0,1]
    scale_image = (image-lo)/(hi-lo)

    #rescale
    width = stop - start
    rescale_image = (scale_image * width) + start

    #hard limits?
    if hard is True:

        if np.any(rescale_image < start):
            bad = (rescale_image < start)
            rescale_image[bad] = start

        if np.any(rescale_image > stop):
            bad = (rescale_image > stop)
            rescale_image[bad] = stop

    return rescale_image

def image_prep(input_arr, hdu, slide, scale, scope = [0, 1000], smooth = True, mask=None):

    '''Returns the scaled and transformed image block to be used for rgb mapping.

    Params
    ------
    input_arr (list of strings of length 3) :
        directory to .fits files eg. ['i.fits', 'r.fits', 'g.fits']
    slide (list of np.floats of length 3) :
        value to be added to each input_arr dataset eg. [0.1, 0.2, 0.3]
    scale (list of np.floats of length 3) :
        value to divide each dataset. Scales them into a comparable numerical range eg. [90, 80, 50]
    scope (list of np.floats of length 2) :
        range of values from which the data is transformed to [1, 1000]. Values which lie outside
        eg. [scopelo, scopehi] are clipped to the boundary value


    Returns
    -------
    abblock (list of np.ndarray) :
        i, r and g data slided and scaled to [1, 1000]
    abtot (np.ndarray) :
        a total image adding i, g and r np.ndarrays in abblock
    imtot (np.ndarray) :
        a total image adding input_arr data

    '''

    # initialize arrays for filling
    imblock = []
    abblock = []
    ablo = np.zeros(3)
    abhi = np.zeros(3)

    #loop over the input_arr data files for sliding and scaling
    for i in range(0, len(input_arr)):

        # data
        try:
            hdulist = fits.open(input_arr[i])
            img_data = None
            hdu_index = hdu

            while img_data is None:
                # Extract image data from file and close
                try:
                    img_data = hdulist[hdu_index].data
                    hdu_index += 1

                except IndexError:
                    print("Could not find image data in file.")
                    hdulist.close()
                    sys.exit(1)

            hdulist.close()

            if img_data.dtype != np.double:
                img_data = img_data.astype(np.float)

        except IOError:
            print("Could not read file:", input_arr[i])
            sys.exit(1)


        if smooth:
            img_data = cv2.GaussianBlur(img_data, (9,9),0)
            #sdata[np.isnan(mask)] = img_data[np.isnan(mask)]
            #img_data = sdata

        imblock.append(img_data)

        # contour data
        if (i == 0):
            cont_tot = img_data
        else:
            cont_tot = cont_tot + img_data

        # apply slide
        imdat = img_data + slide[i]

        # apply scale
        imdat = imdat / scale[i]

        # rescale to cut levels and construct an absolute image [0,1000]
        scopelo = scope[0]
        scopehi = scope[1]

        abdat = ascale(imdat, start = 0., stop = 1000., lo = scopelo, hi = scopehi, hard = True)

        abblock.append(abdat)
        ablo[i] = scopelo
        abhi[i] = scopehi

        # construct total image
        if (i == 0):
            imtot = imdat
            abtot = abdat
        else:
            imtot = imtot + imdat
            abtot = abtot + abdat


    imtot = imtot / np.float(len(input_arr))
    abtot = abtot / np.float(len(input_arr))
    cont_tot = cont_tot / np.float(len(input_arr))

    return abblock, abtot, imtot

def image_process(image, lo=0, hi=1000):

    '''Applies chosen mapping function (log10) to the image data and
    rescales it to range 0-1.

    Params
    ------
    image (np.ndarray) :
        image data to be rescaled
    lo (np.float) :
        lower cut reference level of original image data
    hi (np.float) :
        upper cut reference level of original image data

    Returns
    -------
    rescaled (np.ndarray) :
        rescaled data array to 0-1 range

    '''

    # temp file - all data in range [0,1000]
    temp = image

    # apply functions with scaling

    #if (func == 'log'):
    image = np.log10(ascale(temp, start=1., stop=1000., lo=lo, hi=hi, hard=True))
    lofunc = np.log10(1.)
    hifunc = np.log10(hi)
    #else:
    #    raise KeyError("unknown aimage function applied")


    # rescale image data to range 0-1
    rescaled = ascale(image, start = 0., stop = 1., lo=lofunc, hi=hifunc)

    if np.any(np.isnan(rescaled)) : rescaled[np.isnan(rescaled)] = 0.

    return rescaled


def aimage(input_arr, hdu, lo=0., hi=1000., slide = [0, 0, 0], scale = [40, 28, 20], scope = [0, 75],
            truncatemax = True, smooth = False, mask=None, invert=False, cmap = None, fname= 'output.png'):

    '''The function creates the scaled, transformed image or composite RGB image.

    Params
    ------
    input_arr (list of strings of length 3) :
        directory to .fits files eg. ['i.fits', 'r.fits', 'g.fits']
    slide (list of np.floats of length 3) :
        value to be added to each input_arr dataset eg. [0.1, 0.2, 0.3]
    scale (list of np.floats of length 3) :
        value to divide each dataset. Scales them into a comparable numerical range eg. [90, 80, 50]
    scope (list of np.floats of length 2) :
        range of values from which the data is transformed to [1, 1000]. Values which lie outside
        eg. [scopelo, scopehi] are clipped to the boundary value

    truncatemax (boolean value) :
        impose hard limits? True or False
    invert (boolean value) :
        invert calculated colours

    cmap (string or None) :
        matplotlib.colors.Colormap, optional
        For example, cm.viridis. If None, defaults to the image.cmap rcParam.
        (https://matplotlib.org/examples/color/colormaps_reference.html)

    fname (string or None) :
        Path string to a filename, or a Python file-like object.
        If format is None and fname is a string, the output format is deduced
        from the extension of the filename.

    '''
    abblock, abtot, imtot = image_prep(input_arr, hdu, slide, scale, scope, smooth, mask)

    # run image process function
    outblock = image_process(image=abtot, lo=lo, hi=hi)

    # lupton method (#1) - saturate to colour (Lupton+ 2004, Eq. 2)

    # average total flux
    r = abblock[0]
    g = abblock[1]
    b = abblock[2]
    I = abtot
    fI = outblock

    # three bands
    R = (r / I) * fI
    G = (g / I) * fI
    B = (b / I) * fI

    # limits
    maxarr = np.array([np.nanmax(R), np.nanmax(G), np.nanmax(B)])
    maxrgb = np.nanmax(maxarr)
    if (maxrgb > 1) :
        if (truncatemax):
            R = ascale(R, start = 0., stop = 1., lo = 0., hi = 1., hard = True)
            G = ascale(G, start = 0., stop = 1., lo = 0., hi = 1., hard = True)
            B = ascale(B, start = 0., stop = 1., lo = 0., hi = 1., hard = True)
        else:
            R = R / maxrgb
            G = G / maxrgb
            B = B / maxrgb


    if (np.any(I == 0.)):
        bad = (I == 0.)
        R[bad] = 0.
        G[bad] = 0.
        B[bad] = 0.


    # update outblock
    outblock = np.array([R, G, B])

    # transform to colour scale [1:256], and create averaged image block
    transblock = outblock
    for i in range(0, len(input_arr)):

        # invert colours?
        if (invert):
            outblock[i] = 1 - outblock[i]

        # scale 0:1 -> 1:256
        outblock[i] = (outblock[i] * 255) #+ 1

    # averaged image block
        if (i == 0):
            imsum = outblock[i]
        else:
            imsum = imsum + outblock[i]


    imsum = imsum / np.float(len(outblock)) # scale 1:256

    # return colour
    if (len(outblock) == 3):
        red = outblock[0]
        green = outblock[1]
        blue = outblock[2]

    else:
        red = green = blue = imsum

    rgb = np.dstack([red, green, blue]).astype(np.uint8)

    if fname:
        matplotlib.image.imsave(fname, rgb, origin='lower', cmap=cmap)

    return rgb


'''

i = sys.argv[1]
r = sys.argv[2]
g = sys.argv[3]
irg = sys.argv[4]


images = [i, r, g]

aimage(input_arr=images, hdu=1, smooth=False, scale = [45, 35, 20], scope = [0, 70], fname=irg)
'''

'''
sdir = 'Users/princess/Desktop/astrospheres/images-stripe/f0895_stripe82/'
images = [sdir + 'i.fits', sdir + 'r.fits', sdir + 'g.fits']
aimage(input_arr=images, hdu=0, smooth=False, scale = [50, 35, 20], scope = [0, 100], fname= sdir + 'IRG.png')

'''
sdir = '/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source/'
#images = [sdir + 'i.fits', sdir + 'r.fits', sdir + 'g.fits']
aimage([sdir + 'f1213_i.rec_subtracted_it2_it3.fits', sdir + 'f1213_r.rec_subtracted_it2_it3.fits', sdir + 'f1213_g.rec_subtracted_it2_it3.fits'], hdu=0, smooth=False, scale = [50, 35, 20], scope = [0, 100], fname= sdir + 'IRG.png')
