# Deep image
#
# This make file is written to build the deep image by combining g,r,i images with a mean.


#
# Original author:
# Giulia Golini  <giulia.golini@gmail.com>
# Contributing author(s): Giulia Golini  <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

.ONESHELL:


all: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_deep.txt

/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_deep.fits: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source/f1213_g.rec_subtracted_it2_it3.fits /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source/f1213_r.rec_subtracted_it2_it3.fits /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/Source/f1213_i.rec_subtracted_it2_it3.fits
	astarithmetic $(word 1,$^) --hdu=0 $(word 2,$^) --hdu=0 $(word 3,$^) --hdu=0 3 mean --output=$@




/Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_deep.txt: /Users/princess/Desktop/astrospheres/images-stripe/f1213_stripe82/f1213_deep.fits
	echo $< >  $@
