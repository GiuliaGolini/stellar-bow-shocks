# Angular size depending on the distance
#
# This script is written to compute the angular size of different stars with
# radius > 100 AU as a function of the distance.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Giulia Golini.
#
#
# This Python script is free software: you can redistribute it and/or modify
# it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Defining variables
# theta = angular diameter (arcseconds)
# l = linear diameter  (Astronomica Units AU) da 100 AU
# d = distance (AU)

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot

# Direct formula
#l = 11500 
#d = 2500*63241 # 1 ly è 63241 UA
#theta = np.arctan(l/d) * 206265
#theta = (l/d) * 206265

#l = d * (theta/206265)


#print ("theta",theta, "arcsec, l",l,"AU,d",d,"AU")


# l è la dimensione dell'anulus che deve essere 100 AU e 10000 AU
# d = np.linspace( 10, 1000 ,100) # Linear dimensions of a star in Ly, from 10 to 1000 ly as decided
for l in range (100, 10100, 9900): # limite inferiore imposto da Nacho
    d = np.linspace( 10, 100 ,100) # Linear dimensions of a star in Ly, from 10 to 100 ly as decided
    theta = (l*0.00001581/d) * 206265 # 1 AU is 0.00001581 Ly
    plt.plot(d, theta, label = 'l(AU) = ' + str(l))
    plt.legend()
    
 
theta = (100.*0.00001581/150.) * 206265
print ("theta a 100AU",theta, "arcsec")

'''

d = 100
l = np.linspace( 0, 0.007992095804,100 ) # Linear dimensions of a star in AU
theta = (l/d) * 206265
plt.plot(l, theta, color = 'red', label = "distance = 100 AU")
plt.legend()

d = 200
l = np.linspace( 0, 0.007992095804,100 ) # Linear dimensions of a star in AU
theta = (l/d) * 206265
plt.plot(l, theta, color = 'blue', label = "distance = 200 AU")
plt.legend()

d = 300
l = np.linspace( 0, 0.007992095804,100 ) # Linear dimensions of a star in AU
theta = (l/d) * 206265
plt.plot(l, theta, color = 'green', label = "distance = 300 AU")
plt.legend()
'''

plt.title('Angular dimension as a function of distance for different linear dimensions ')
plt.xlabel("Distance (Ly)")
plt.ylabel("log(theta) (arcseconds)")
plt.yscale('log')    # Convert in log scale

plt.show()











