# Download Gaia catalog in Stripe 82 area of sky
#
# This Python script downloads datas from a skycatalog in a given region of the sky,
# using Vizier. To more informations I send you to the links :
# https://astroquery.readthedocs.io/en/latest/vizier/vizier.html
# http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=V/147
# This script takes as inputs the coordinates of the center of the region of sky to looking
# for, the width and the height of the region and the catalog selected.
#
# This script permits to build a catalog from Gaia2 database which contains the stars that appears
# in the Stripe82 Legacy project: the output catalog cover an area of 275 square degrees
# (-50º < R.A. < 60º, -1.25º < Dec- < 1.25º).
#
#
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Giulia Golini.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





#------------------------------------------------------------------------------------------------------------------
# USAGE:
#-----------------------------------------------------------------------------------
# python3 download-catalogue.py RA  DEC  width height catalog_name catalog_output_name
#
# PARAMETERS:
# -----------
# RA		          =   ra                      =   Right ascension coordinates in degrees
# DEC	              =   dec                     =   Declination coordinates in degrees
# width	              =   width                   =   Stars will be searched in the box with this width in minutes
# height              =   height                  =   Stars will be searched in the box with this height in minutes
# catalog_name        =   catalog_name            =   Input name of the catalog with all objects
# catalog_output_name =   catalog_output_name     =   Output name of the catalog with all objects
# ------------------------------------------------------------------------------------------------------------------



# result = Vizier.query_region(coord.SkyCoord(ra=20.86418538, dec=-0.6229532711,unit=(u.deg, u.deg),frame='icrs'),width="3m",height="4m",catalog=["SLOAN"])
# python3.6 download-catalog.py 5. 0. 110m 2.5m I/345/gaia2 gaiacat

#Query a region
import sys
from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord

# Define the arguments (input and output image names)
ra = sys.argv[1]
dec = sys.argv[2]
width = sys.argv[3]
height = sys.argv[4]
catalog_name = sys.argv[5]
catalog_output_name = sys.argv[6]

ra = float(ra)
dec= float(dec)


# Query the region
Vizier.ROW_LIMIT = -1

# width è sulla declinazione, in verticale!!! quindi i 2.5 gradi di Stripe
# height è sull'ascensione retta in teoria
# download-catalog.py 46. 0. 150m "1650m" I/345/gaia2 gaiacat                              I/337/gaia

result = Vizier.query_region(coord.SkyCoord(ra=ra, dec=dec,unit=(u.deg, u.deg) ,frame='icrs'), width=width,height=height ,catalog=[catalog_name])


cat = result[catalog_name]


# Save only 5 columns of the catalog, with right ascension, declination, g magnitude, extinction and parallax.

newcat = cat['RA_ICRS','DE_ICRS','Gmag','AG','Plx']
cat.write(catalog_output_name, format='fits', overwrite=True)




'''
#result = Vizier(catalog="I/345/gaia2", column_filters={'DE_ICRS':'<1.25','DE_ICRS':'>-1.25' }).query_region_async(coord.SkyCoord(ra=5., dec=0.,unit=(u.deg, u.deg) ,frame='icrs'), width="10m",height="10m" )

python3.6 download-catalog.py 5. 0. 150m 6600m I/345/gaia2 gaiacat



catalog_list = Vizier.find_catalogs('gaia2')
print({k:v.description for k,v in catalog_list.items()})
#'I/345': 'Gaia DR2 (Gaia Collaboration, 2018)'


catalogs = Vizier.get_catalogs(catalog_list.keys())
print(catalogs)

#gaia= catalogs['I/345/gaia2']
#print(gaia)


# Query the region
Vizier.ROW_LIMIT = 500  #(-1)
result = Vizier.query_region(coord.SkyCoord(ra=5., dec=0.,unit=(u.deg, u.deg) ,frame='icrs'), width="110m",height="2.5m",catalog=['I/345/gaia2'])

print(result)
print(result['I/345/gaia2'])

# Save the catalog as .fits table
gaia = result['I/345/gaia2']


# Save only 3 columns of the catalog, with right ascension, declination and g magnitude
gaianew = gaia['RA_ICRS','DE_ICRS','Gmag','AG']
gaianew.write(catalog_output_name, format='fits', overwrite=True)



catstripe = 5. 0. 110m 2.5m I/345/gaia2 gaiacat



result = Vizier.query_region(coord.SkyCoord(ra=46.25 , dec=0.,unit=(u.deg, u.deg) ,frame='icrs'), width="150m",height="1650m",catalog=['I/345/gaia2'])


result = Vizier.query_region(coord.SkyCoord(ra=-8.75, dec=0.,unit=(u.deg, u.deg) ,frame='icrs'), width="150m",height="1650m",catalog=['I/345/gaia2'])

result = Vizier.query_region(coord.SkyCoord(ra=18.75, dec=0.,unit=(u.deg, u.deg) ,frame='icrs'), width="150m",height="1650m",catalog=['I/345/gaia2'])

result = Vizier.query_region(coord.SkyCoord(ra=-36.25, dec=0.,unit=(u.deg, u.deg) ,frame='icrs'), width="150m",height="1650m",catalog=['I/345/gaia2'])


python3.6 download-catalog.py 46.25 0. 150m 1650m I/345/gaia2 gaiacat46
python3.6 download-catalog.py 18.75 0. 150m 1650m I/345/gaia2 gaiacat18
python3.6 download-catalog.py 351.25 0. 150m 1650m I/345/gaia2 gaiacat351



python3.6 download-catalog.py 316.875 0. 150m 825m I/345/gaia2 gaiacat316
python3.6 download-catalog.py 330.625 0. 150m 825m I/345/gaia2 gaiacat330


'''







